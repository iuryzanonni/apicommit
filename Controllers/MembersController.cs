﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using APICommit.Models;

namespace APICommit.Controllers
{
    [Route("api/[controller]/{id}")]
    [ApiController]
    public class MembersController : ControllerBase
    {
        public List<Results> Get(string id)
        {
            Results result = new Results();
            return result.membros(id);
        }
    }
}
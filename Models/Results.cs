﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace APICommit.Models
{
    public class Results
    {
        public string matricula { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        //public string error { get; set; }
        public Results(string matricula, string nome, string email, string error = null)
        {
            this.matricula = matricula;
            this.nome = nome;
            this.email = email;
            //this.error = error;
        }

        public Results()
        {
        }

        public List<Results> membros(string id)
        {
            MySqlConnection conn = ConnectionBD.conn();
            MySqlCommand query = conn.CreateCommand();
            if(id != "all")
                query.CommandText = "SELECT * FROM site_commit.membros_ativos WHERE matricula =" + id;
            else
                query.CommandText = "SELECT * FROM site_commit.membros_ativos";
            //query.Parameters.AddWithValue("@matricula",id);

            var results = new List<Results>();
            conn.Open();

            MySqlDataReader fetch_query = query.ExecuteReader();

            while (fetch_query.Read())
            {
                results.Add(new Results(fetch_query["matricula"].ToString(), fetch_query["nome"].ToString(), fetch_query["email"].ToString()));
            }

            return results;           
        }

    }
    
}
